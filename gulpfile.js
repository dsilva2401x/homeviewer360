var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var watch = require('gulp-watch');

gulp.task('watch', function () {

	watch('**/*.*', function () {
        browserSync.reload();
    });

});

gulp.task('serve', ['watch'], function (watch) {
	browserSync.init({
		server: {
			baseDir: "./"
		}
    });
});