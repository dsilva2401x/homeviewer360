(function(ang) {

	var app = ang.module('app');

	app.controller('listController', ['$scope', '$mdSidenav', '$state', '$resources', function( $scope, $mdSidenav, $state, $resources ) {
		if (!$scope.models) $scope.models = {};
		if (!$scope.methods) $scope.methods = {};

		// Methods
			$scope.methods.addMoreItems = function () {
				for( var i=0;i<5;i++){
					$scope.models.buildings.push({
						id: 1,
						details: 'Casa de 3 pisos nueva',
						city: 'Arequipa',
						district: 'Cayma',
						price: 500000,
						area: 300,
						nrooms: 4,
						nbaths: 2,
						photo: 'http://o.homedsgn.com/wp-content/uploads/2013/02/a-house-19-800x548.jpg'
					});
				}
			}

		// Init
			$scope.models.buildings = [];
			$scope.methods.addMoreItems();

	}]);

})(angular)