(function(ang) {

	var app = ang.module('app');

	app.filter('equiFilter', function() {
		return function(data) {
			return data;
		}
	})

	app.factory('downloadViewData', function($http, $filter) {
		return function(viewId, callback) {
			console.log('Downloading data for view-'+viewId+'..');
			$http({
				method: 'GET',
				url: 'demos/view'+viewId+'.json'
			}).success(function(data) {
				// Filter data
				data = $filter('equiFilter')(data);
				data.adjViews.forEach(function(v) {
					v.img = new Image;
					v.img.src = v.imgSrc;
				})
				// Return data
				callback(data);
			});
		};
	});

	app.factory('EquiTour', function(downloadViewData) {
		return function(config) {

			// Variables
				var container, equiviewrFront, equiviewrBack, 
				self, containerBack, containerFront;
				self = this;

			// Methods
				var init = function() {
					container = config.container;
					containerBack = document.createElement('div');
					containerFront = document.createElement('div');
					containerBack.style['position'] = 'absolute';
					containerBack.style['left'] = '0px';
					containerBack.style['top'] = '0px';
					containerBack.style['width'] = '100%';
					containerBack.style['height'] = '100%';
					containerFront.style['position'] = 'absolute';
					containerFront.style['left'] = '0px';
					containerFront.style['top'] = '0px';
					containerFront.style['width'] = '100%';
					containerFront.style['height'] = '100%';
					containerFront.style['-webkit-transition-duration'] = '0.5s';
					containerFront.style['-moz-transition-duration'] = '0.5s';
					containerFront.style['-o-transition-duration'] = '0.5s';
					containerFront.style['-ms-transition-duration'] = '0.5s';
					container.appendChild(containerBack);
					container.appendChild(containerFront);
					equiviewrFront = new EquiViewr({
						container: containerFront,
						limits: {
							minY: -30
						}
					});
					equiviewrBack = new EquiViewr({
						container: containerBack,
						limits: {
							minY: -30
						}
					});
				}

				var changeView = function(newView) {
					// @1 Change back image and set it to zoom and position of new view
					// @3 Start soft transition to back image
					// @4 Update front image
					// @5 Zoom soft reset in front image

					containerFront.style['-webkit-transition-duration'] = '0.5s';
					containerFront.style['-moz-transition-duration'] = '0.5s';
					containerFront.style['-o-transition-duration'] = '0.5s';
					containerFront.style['-ms-transition-duration'] = '0.5s';

					equiviewrBack.updateImage(newView.imgSrc);
					equiviewrBack.moveTo({
						x: (newView.xDirToCurrent+180)%360,
						// x: newView.xDirToCurrent,
						y: 0
					});
					equiviewrFront.zoomIn(function() {
						containerFront.style['opacity'] = 0;
						setTimeout(function() {
							equiviewrFront.updateImage(newView.imgSrc);
							equiviewrFront.moveTo({
								x: (newView.xDirToCurrent+180)%360,
								// x: newView.xDirToCurrent,
								y: 0
							});
							equiviewrFront.resetZoom();
							setTimeout(function() {
								containerFront.style['-webkit-transition-duration'] = '0.5s';
								containerFront.style['-moz-transition-duration'] = '0.5s';
								containerFront.style['-o-transition-duration'] = '0.5s';
								containerFront.style['-ms-transition-duration'] = '0.5s';
								containerFront.style['opacity'] = 1;
							},200);
							// Add targets
							newView.adjViews.forEach(function(v) {
								equiviewrFront.addTarget(v.xDir.fromCurrent, {
									title: v.title,
									action: function() {
										// Download
										downloadViewData(v.id, function(data) {
											data.xDirToCurrent = v.xDir.fromTarget;
											changeView(data);
										});
									}
								})
							})
						},700);
					});
				}

				this.initView = function(data) {
					// Update view
					equiviewrFront.updateImage(data.imgSrc);
					data.adjViews.forEach(function(view) {
						equiviewrFront.addTarget(view.xDir.fromCurrent, {
							title: view.title,
							action: function() {
								// Download
								downloadViewData(view.id, function(data) {
									data.xDirToCurrent = view.xDir.fromTarget;
									changeView(data);
								});
							}
						})
					})
				}

			// Construct
				init();

		};
	});


	app.directive('equiviewr', function(downloadViewData, EquiTour) {
		return {
			restrict: 'EA',
			link: function( scope, elem, attrs ) {
				if (!scope.models) scope.models = {};

				console.log( elem[0].parentNode.offsetWidth )
				console.log( elem[0].parentNode.offsetHeight )

				elem[0].parentNode.style.overflow = 'hidden';
				var eqvContainer = document.createElement('div');
				eqvContainer.style.position = 'absolute';
				eqvContainer.style.left = '0px';
				eqvContainer.style.top = '0px';
				eqvContainer.style.width = elem[0].parentNode.offsetWidth+'px';
				eqvContainer.style.height = elem[0].parentNode.offsetHeight+'px';
				eqvContainer.style.background = 'grey';
				elem[0].appendChild(eqvContainer);
				console.log( eqvContainer );

				// Instance equiTour
				scope.models.equiTour = new EquiTour({
					container: eqvContainer
				});
			}
		}
	});

})(angular)