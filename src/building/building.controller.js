(function(ang) {

	var app = ang.module('app');

	app.controller('buildingController', ['$scope', '$mdSidenav', '$state', '$resources', 'downloadViewData', function( $scope, $mdSidenav, $state, $resources, downloadViewData ) {
		if (!$scope.models) $scope.models = {};
		if (!$scope.methods) $scope.methods = {};

		// Models
			$scope.models.building = {
				id: 1,
				details: 'Casa de tres pisos',
				address: 'Urb. Primavera A-9',
				city: 'Arequipa',
				district: 'Cayma',
				price: 500000,
				area: 300,
				nrooms: 4,
				nbaths: 2
			};
			downloadViewData($scope.models.building.id, function (data) {
				$scope.models.equiTour.initView(data);
			});
			

	}]);

})(angular)