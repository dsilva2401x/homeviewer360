(function(ang) {

	var app = ang.module('app');

	app.config(['$mdThemingProvider', '$resourcesProvider', '$stateProvider', function ($mdThemingProvider, $resourcesProvider, $stateProvider) {

		// Material theme
			$mdThemingProvider.theme('default').primaryPalette('purple', {
				'default': '900',
				'hue-1': '700',
				'hue-2': '500',
				'hue-3': '200',
			});

		// Resources
			$resourcesProvider.init({
				resources: {

					Me: {
						// route: '/api/me'
						route: 'demos/me.json'
					}

				}
			})

		// Routes
			$stateProvider
				.state('search', {
					url: '/search',
					templateUrl: 'src/search/search.html'
				})
				.state('list', {
					url: '/list',
					templateUrl: 'src/list/list.html',
					controller: 'listController'
				})
				.state('building', {
					url: '/building/:buildingId',
					templateUrl: 'src/building/building.html',
					controller: 'buildingController'
				})

	}]);

})(angular)