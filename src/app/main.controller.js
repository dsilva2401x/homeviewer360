(function(ang) {

	var app = ang.module('app');

	app.controller('appContainer', ['$scope', '$mdSidenav', '$state', '$resources', function( $scope, $mdSidenav, $state, $resources ) {
		if (!$scope.models) $scope.models = {};
		if (!$scope.methods) $scope.methods = {};

		// Attributes
			$scope.models.config = {
				consoleName: 'HomeViewer',
				panelName: 'Opciones'
			}

			$scope.models.panel = [
				{
					name: '',
					options: [
						{
							name: 'Buscar',
							state: 'search'
						},
						{
							name: 'Inmuebles',
							state: 'list'
						}
					]
				}
			];

		// Methods
			$scope.methods.openLeftSideNav = function () {
				$mdSidenav('left').open();
			}

			$scope.methods.closeLeftSideNav = function () {
				$mdSidenav('left').close();
			}

			$scope.methods.openRightSideNav = function () {
				$mdSidenav('right').open();
			}

			$scope.methods.goToState = function (stateName) {
				$state.go(stateName);
				$mdSidenav('left').close();
			}

		// Init
			

	}]);

})(angular)